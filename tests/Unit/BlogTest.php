<?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_a_blog_post_can_be_created()
    {
        $post = factory(Post::class)->make();

        $this->assertInstanceOf('App\Post', $post);
    }

    public function test_posts_can_be_viewed() {

        $post = factory(Post::class)->create();

        $this->get('/blog')
            ->assertSee($post->title);
    }
    public function test_a_post_can_be_viewed_individually() {

        $post = factory(Post::class)->create();

        $this->get($post->path())
            ->assertSee($post->title)
            ->assertSee($post->body);
    }
    public function test_a_post_can_be_submitted() {
        $post = Post::create([
            'title' => 'New post',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui eos repudiandae maxime, aut officiis aliquam nobis dolores voluptatum magni ipsam eaque non, at culpa tempora ad blanditiis illum est excepturi adipisci laboriosam officia autem enim. Odit sunt iure hic, molestiae molestias modi commodi, eos, suscipit cum aut error culpa excepturi!'
        ]);

        $this->post('/blog', $post->toArray());

        $this->get($post->path())
            ->assertSee($post->title)
            ->assertSee($post->body);
    }
}
