<?php

namespace Tests\Feature;

use App\Project;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_projects_can_be_viewed() {

    	$project = factory('App\Project')->create();

    	$photo = factory('App\Photo')
	    	->create([  
	    		'project_id' => $project->id
	    	]);
	    $photo = factory('App\Photo')
	    	->create([  
	    		'project_id' => $project->id
	    	]);

    	$this->withoutExceptionHandling()
	    	->get('/projects')
	    	->assertSee($project->company)
	    	->assertSee($project->url)
	    	->assertSee($project->description)
	    	->assertSee($photo->alt);
    }
}
