import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		component: require('../../views/home'),
	},
	{
		path: '/about',
		component: require('../../views/about'),
	},
	{
		path: '/blog',
		component: require('../../views/blog/index'),
	},
	{
		path: '/blog/create',
		component: require('../../views/blog/create'),
	},
	{
		path: '/blog/:id',
		component: require('../../views/blog/show'),
	},
	{
		path: '/blog/:id/edit',
		component: require('../../views/blog/edit'),
	},
	{
		path: '/projects',
		component: require('../../views/projects/index')
	},
	{
		path: '/projects/create',
		component: require('../../views/projects/create')
	},
	{
		path: '/projects/:id',
		component: require('../../views/projects/show')
	},
	{
		path: '/projects/:id/edit',
		component: require('../../views/projects/edit')
	}
];

export default new VueRouter({

	routes

});