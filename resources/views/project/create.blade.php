@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card-body">

		<div class="card w-75 mx-auto">
			<div class="card-header">Create a new project</div>
			<div class="card-body">
				<form action="/projects" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="title">Company Name</label>
						<input class="form-control" type="text" name="company">
					</div>
					<div class="form-group">
						<label for="title">Company Url</label>
						<input class="form-control" type="text" name="url">
					</div>
					<div class="form-group">
						<label for="body">Project Description</label>
						<textarea class="form-control" name="description" id="" rows="10"></textarea>
					</div>
					<div class="form-group">
						<label for="photo">Upload a photo</label>
						<input class="form-control" type="file" name="photo">
					</div>
					<div class="form-group">
						<label for="alt">Photo Description</label>
						<textarea name="alt" id="" cols="" rows="2" class="form-control"></textarea>
					</div>

					<button class="btn btn-primary" type="submit">Submit</button>

				</form>
			</div>
		</div>

	</div>
</div>
@endsection