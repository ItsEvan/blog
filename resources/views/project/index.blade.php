@extends('layouts.app')

@section('content')
<div class="container">
	@foreach($projects as $project)
	<div class="card mt-2 mx-auto w-75">
		<div class="card-body">
			<h2 class="card-title"><a href="{{$project->path()}}">{{ $project->company }}</a></h2>
			<h5>{{ $project->url }}</h5>
			<p>{{ $project->description }}</p>
			@foreach($project->pictures as $photo)
				<img src="{{ $photo->path() }}" alt="{{ $photo->alt }}">
			@endforeach

		</div>
	</div>
	@endforeach
</div>
@endsection