<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Evan Ling, Web Developer</title>

	<!-- Scripts -->
	<script src="/js/app.js" defer></script>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Muli:300|Roboto+Slab" rel="stylesheet">  
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app">
		<nav>
			<div class="left-nav">
				<router-link to="/">
					<img class="left-nav__logo" src="/img/logo.svg" alt="Letters E and L with a colon"/>
					<h2 class="left-nav__nav-link" style="font-weight: normal">
						evan ling
					</h2>
				</router-link>
				<!-- Left Side Of Navbar -->
				<ul>
					@auth
					<a class="left-nav__logout" href="{{ route('logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
					@endauth
				</ul>
			</div>

			<!-- Right Side Of Navbar -->
			<ul class="right-nav">
				<li><router-link to="/about" class="nav-link">About</router-link></li>
				<li><router-link to="/blog" class="nav-link">Blog</router-link></li>
				<li><router-link to="/projects" class="nav-link">Projects</router-link></li>
				<li><a href="https://gitlab.com/itsevan" class="nav-link">Gitlab</a></li>
			</ul>
		</nav>

		<main>
			@yield('content')
				<transition name="slide">
					<router-view></router-view>
				</transition>
		</main>
	</div>
</body>
</html>
