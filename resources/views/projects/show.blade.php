@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card mt-2 mx-auto w-75">
		<div class="card-body">
			<h2 class="card-title">{{ $project->company }}</h2>
			<h5>{{ $project->url }}</h5>
			<p>{{ $project->description }}</p>
			@foreach($project->pictures as $photo)
				<p>{{ $photo->name }}</p>
				<img src="{{ $photo->path() }}" alt="{{ $photo->alt }}">
			@endforeach

		</div>
	</div>
</div>
@endsection