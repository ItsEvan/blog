@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card-body">

		<div class="card w-75 mx-auto">
			<div class="card-header">Edit a post</div>
			<div class="card-body">
				<form action="/projects/{{ $project->id }}" method="post" enctype="multipart/form-data">
					<input type="hidden" name="_method" value="patch">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="company">Company</label>
						<input class="form-control" type="text" name="company" value="{{ $project->company }}">
					</div>
					<div class="form-group">
						<label for="url">Project URL</label>
						<input class="form-control" type="text" name="url" value="{{ $project->url }}">
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea class="form-control" name="description" id="" rows="10">
							{{ $project->description }}
						</textarea>
					</div>
					@foreach($project->pictures as $photo)
						<img src="/{{ $photo->url}}" alt="photo here" width="200px" height="200px">
					@endforeach
					<div class="form-group">
						<label for="photo">Photo</label>
						<input type="file" name="photo" class="form-control">
					</div>
					<div class="form-group">
						<label for="alt">Alt Text</label>
						<textarea class="form-control" name="alt" id="" rows="10">{{ $project->description }}</textarea>
					</div>

					<button class="btn btn-primary" type="submit">Submit</button>
				</form>
				<form action="/projects/{{ $project->id }}" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="DELETE">
					<button type="submit" class="mt-4 btn btn-danger">Delete Project</button>
				</form>
			</div>
		</div>

	</div>
</div>
@endsection