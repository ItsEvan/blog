@extends('layouts.app')

@section('content')
<h1>{{ __('Login') }}</h1>

<form method="POST" action="{{ route('login') }}">
    @csrf

    <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

    @if ($errors->has('email'))
    <span class="invalid-feedback">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
    @endif

    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

    @if ($errors->has('password'))
    <span class="invalid-feedback">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
    @endif

    <label>
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
    </label>

    <button type="submit" class="btn btn-primary">
        {{ __('Login') }}
    </button>

</form>
@endsection
