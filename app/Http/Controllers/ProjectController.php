<?php

namespace App\Http\Controllers;

use App\Project;
use App\Photo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {

        $this->middleware('auth')->except(['index', 'show']);

    }    

    public function index()
    {
        $projects = Project::latest()->with('pictures')->get();

        return $projects;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $project = Project::create([
            'company' => request('company'),
            'url' => request('url'),
            'description' => request('description')
        ]);

        $path = $request->photo->store('public');


        $photo = Photo::create([
            'name' => substr($path, 7),
            'alt' => request('alt'),
            'url' => request('url'),
            'project_id' => $project->id
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Project::with('pictures')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return Project::with('pictures')->find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $project = Project::find($id);


        $project->company = $request->company;
        $project->url = $request->url;
        $project->description = $request->description;

        $project->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $project = Project::find($id);

        Project::destroy($project->id);

        return redirect('/projects');

    }
}