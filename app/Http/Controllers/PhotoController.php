<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Photo;
use App\Project;


class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::find($request->project_id);

        if ($request->photo) {
            $photos = Photo::where('project_id', $project->id)->get();
            foreach($photos as $photo) {
                $path = Storage::putFile('public', $request->file('photo'));
                Storage::delete('public/' . $photo->name);
                $photo->name = substr($path, 7);
                $photo->alt = $photo->alt;
                $photo->save();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $project = Project::find($id);

        if ($request->photo) {
            $photos = Photo::where('project_id', $project->id)->get();
            foreach($photos as $photo) {
                $path = Storage::putFile('public', $request->file('photo'));
                Storage::delete('public/' . $photo->name);
                $photo->name = substr($path, 7);
                $photo->alt = request('alt');
                $photo->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::find($id);

        Storage::delete('public/' . $photo->name);
        Photo::destroy($photo->id);

    }
}
