<?php

namespace App;

use App\Project;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

	protected $guarded = [];

	public function path() {
		return "{$this->name}";
	}

}
