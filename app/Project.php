<?php

namespace App;

use App\Photo;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

	protected $guarded = [];

	public function pictures() {
		return $this->hasMany(Photo::class);
	}

	public function path() {
		return "/projects/{$this->id}";
	}
}
