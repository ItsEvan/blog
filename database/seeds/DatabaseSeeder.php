<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\Post::class, 10)->create();

    	App\User::create([
    		'name' => 'Evan Ling',
    		'password' => bcrypt('one1won11'),
    		'email' => "evanling@tuta.io"
    	])->save();
    }
}
