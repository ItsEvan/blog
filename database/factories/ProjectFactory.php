<?php

use App\Project;

use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'company' => 'Evan Ling',
        'url' => 'evanling.me',
        'description' => $faker->sentence()
    ];
});
